# linear response example

A simple LJ system is perturbed by a weak (or strong) quadratic potential around z = 10 nm.
The resulting density profile can be predicted from linear response theory, see Theory of simple liquids, Hanson & McDonald.

## Requirements

For the notebook you need:

- Matplotlib
- Numpy
- Scipy

and JupyterLab or similar to run it.

For running the simulations you need GROMACS (2018 or newer, I guess).
