#!/bin/bash
set -euo pipefail

# equilibrations
for equifolder in perturbed-k*/equi; do
    pushd $equifolder
    gmx -quiet grompp -p ../topol/topol.top -r ../topol/restraint.gro
    gmx -quiet mdrun -v
    popd
done

# production runs
for prodfolder in *perturbed*/prod; do
    pushd $prodfolder
    gmx -quiet grompp -p ../topol/topol.top -c ../equi/confout.gro -r ../topol/restraint.gro
    gmx -quiet mdrun -v
    popd
done

# density profiles
for prodfolder in *perturbed*/prod; do
    pushd $prodfolder
    gmx -quiet density -dens number -sl 92 -f traj_comp.xtc <<< "0"
    popd
done

# RDF
for prodfolder in unperturbed/prod; do
    pushd $prodfolder
    gmx -quiet rdf -bin 0.2 -f traj_comp.xtc -ref all -sel all -b 0 -e 2
    popd
done

# cleanup backup files an trajectory
for folder in *perturbed*/{prod,equi}; do
    pushd $folder
    rm -f \#* traj_comp.xtc
    popd
done
